\chapter{Mathematical Model}

\section{Correlation network of the multiphase system}

\begin{figure}
  \centering
  \includegraphics[width=0.9\textwidth]{figures/corr_network.pdf}  
  \caption{The correlation network of the multiphase system}
  \label{fig::corr_network}
\end{figure}

\noindent The correlation network of the multiphase system is demonstrated in Fig. \ref{fig::corr_network}, which is a lucid introduction to the mathematical models in this work. The major physical candidates in the process and their ways of communication are interpreted. 

\noindent In the simulations that this work considers, the flow field, bubble, and external force are active participants. The dynamics of the flow field itself can be captured by the mathematical models in section 2.2. The bubble that we investigate has two main features, namely the shape and the position, described by the mathematical models in sections 2.3 and 2.4, respectively. The external force can be gravitational or electromagnetic. Gravity is considered in this work, which influences the bubble position through the buoyancy effect.

\noindent While the bubble motion is manipulated by the flow field through the drag, the lift, and the added mass forces, the bubble also gives back a reaction force on the ambient flow field in the two-way coupled case. The bubble geometry is influenced by the flow field due to the strain and vorticity effects, and by the surface tension, which derives from the shape of the bubble itself. Although the shape of the bubble does not have a direct effect on the flow field, it alters the projected area normal to the drag and the lift direction in the drag and the lift terms of the motion equation, so the bubble motion can be altered due to its deformability. This can indirectly lead to further changes in the flow field due to the reaction force of the dispersed phase on the flow field in the two-way coupled case, which equates to a pseudo-two-way coupling between the bubble geometry and the flow field.


\section{Governing equations of fluid motion}

\subsection{Incompressible Navier-Stokes equations}

\noindent The incompressibility of the flow leads to a solenoidal velocity field with
\begin{align}
\nabla \cdot \boldsymbol{u}=0.
\end{align}

\noindent where the variable $\boldsymbol{u}$ is the flow velocity vector [Batchelor 1967], which demonstrates the conservation of mass in the incompressible Navier-Stokes equations (NSE). The conservation form of the momentum equation in incompressible NSE reads
\begin{align}
\frac{\partial \boldsymbol{u}}{\partial t}+(\boldsymbol{u} \cdot \nabla) \boldsymbol{u} = -\frac{1}{\rho} \nabla p+\nu \nabla^2 \boldsymbol{u}+\boldsymbol{f}.
\end{align}

\noindent where $p$ is the pressure, $\rho$ is the fluid density, $\nu$ is the fluid viscosity, $\boldsymbol{f}$ is the external force per unit mass, and $\nabla$ is the gradient operator. The equation describes the acceleration of the fluid particles. Its left-hand side represents the time derivative of the velocity vector $\boldsymbol{u}$ and the convective acceleration of the fluid, namely the acceleration due to the motion of the fluid itself. The right-hand side represents the pressure gradient, viscous forces, which results from the friction between adjacent fluid layers, and external forces such as gravity or electromagnetic fields [Batchelor 1967].

\noindent The assumption of incompressibility precludes the occurrence of density and pressure waves, such as sound or shock waves, rendering this simplification impractical for investigating these phenomena. However, the incompressible flow assumption applies to all fluids at low Mach numbers (e.g., up to approximately Mach 0.3) and is well-suited for modeling air flows under normal temperatures, particularly in the context of atmospheric wind modeling [Acheson (1990)].

\subsection{Kinetic theory and Boltzmann equation}

\noindent The particle distribution function $f(\boldsymbol{\xi}, \boldsymbol{x}, t)$, which is the fundamental variable in kinetic theory, represents the density of the matter in both three-dimensional physical space at the coordinate $\boldsymbol{x}$, in three-dimensional velocity space with the particle velocity $\boldsymbol{\xi}$, and at time point $t$. 

\noindent The temporal evolution of $f(\boldsymbol{\xi}, \boldsymbol{x}, t)$ can be described by the Boltzmann equation
\begin{align}
\frac{\partial f}{\partial t}+\boldsymbol{\xi} \cdot \nabla_x f+\frac{\boldsymbol{F}}{\rho} \cdot \nabla_{\xi} f=\Omega(f).
\end{align}

\noindent The first two terms on the left-hand side denote the advection of the distribution function by the velocity of its constituent particles $\boldsymbol{\xi}$. The third term describes the effect of external forces (such as gravity or electromagnetic fields) on the distribution function. The source term on the right-hand side is the collision operator, which represents the effects of particle-particle interactions on the distribution function []. The collision operators employed in LBM are typically derived from the relatively simpler BGK collision operator (Eq. (\ref{eq::bgk})), invented by Bhatnagar, Gross, and Krook [].
\begin{align}
\Omega(f)=-\frac{1}{\tau}\left(f-f^{eq}\right)
\label{eq::bgk}
\end{align}

\noindent The relaxation time $\tau$ determines the evolution speed of the distribution function towards the equilibrium distribution $f^{eq}$, and its value correlates with the transport coefficients such as viscosity and heat diffusivity []. The equilibrium distribution $f^{eq}$ reads
\begin{align}
f^{eq}(\rho, \boldsymbol{u}, T, \boldsymbol{\xi})=\frac{\rho}{(2 \pi R T)^{d / 2}} e^{-(\boldsymbol{\xi}-\boldsymbol{u})^2 /(2 R T)}.
\end{align}

\noindent where $\rho$ is the local density, $d$ is the number of spatial dimensions, $R$ is the universal gas constant, and $T$ is the thermodynamic temperature. The physical meaning is that the microscopic particle-particle interaction constantly drives the system toward equilibrium.

\section{Governing equations of bubble morphology}

The morphology of the dispersed phase is based on the phenomenological model developed by Maffettone and Minale []. Under the assumption that the particles are triaxial ellipsoids, the shape and the orientation of the particles can be described by a second-order positive-definite symmetric tensor $\boldsymbol{S}$ (shape tensor). It satisfies the condition $\boldsymbol{S}^{-1}: \boldsymbol{x}\boldsymbol{x}=1$, where $\boldsymbol{x}$ is the position vector of any point on the ellipsoid surface relative to its center. For any known $\boldsymbol{S}$, the eigenvalues $\boldsymbol{\lambda}$ of the shape tensor give the square of the semiaxes of the ellipsoid while the eigenvectors give the orientation of the semiaxes of the ellipsoid. The shape equation reads
\begin{align}
\frac{d \boldsymbol{S}}{d t}-(\boldsymbol{\Omega} \boldsymbol{S}-\boldsymbol{S} \boldsymbol{\Omega})=-\frac{f_1}{\tau}(\boldsymbol{S}-g(\boldsymbol{S}) \boldsymbol{I})+f_2(\boldsymbol{E} \boldsymbol{S}+\boldsymbol{S} \boldsymbol{E}).
\label{eq::shape_function}
\end{align}

\noindent Here $\boldsymbol{\Omega}=0.5\left[\nabla \boldsymbol{u}-\nabla \boldsymbol{u}^{\mathrm{T}}\right]$ and $\boldsymbol{E}=0.5\left[\nabla \boldsymbol{u}+\nabla \boldsymbol{u}^{\mathrm{T}}\right]$ represent the vorticity rate and strain rate tensor, respectively. $\boldsymbol{u}$ is the interpolated flow velocity at the particle position. $\tau=\mu_c R / \sigma$ corresponds to the interfacial relaxation time, where $\mu_c$ is the dynamic viscosity of the carrier phase, $R$ is the radius of the undeformed particle, and $\sigma$ is the interfacial tension. Also, the function 
\begin{align}
g(\boldsymbol{S})=3 \frac{I I I_{s}}{I I_{s}}
\end{align}

\noindent is introduced to preserve the particle volume, which was demonstrated by Maffettone and Minale []. Here,
\begin{align}
&II_s=\lambda_1\lambda_2+\lambda_2\lambda_3+\lambda_3\lambda_1\\
&III_s=\lambda_1\lambda_2\lambda_3
\end{align}

\noindent are the second and third invariant of the shape tensor, respectively, where $\lambda_1$, $\lambda_2$ and $\lambda_3$ are the eigenvalues of a 3$\times$3 matrix. $f_1$ and $f_2$ are rotation and stretching prefactors that are chosen to make the model recover linear asymptotic limits for small capillary number $Ca$ (the ratio of the interfacial relaxation time to the characteristic flow time). According to previous studies [][][][][][], besides the dependency of the prefactors on the viscosity ratio $\hat{\mu}=\mu_p / \mu_c$ ($p$ for particle and $c$ for carrier), $f_2$ depends increasingly on $Ca$ with $Ca > 0.3$, and both prefactors become dependent on $R$/$h$ ($R$ for undeformed bubble radius and $h$ for confinement length scale) when the bubble is relatively large in comparison to the shearing walls i.e. $R$/$h > 0.15$. Similar to the study of Spandan et al.[], with $Ca \ll 1$ in this study, the correction of the prefactor which includes an additional dependence of $f_2$ on $Ca$ is barely necessary. Besides, since only Kolmogorov-sized particles are considered in this case, which corresponds to a locally linearized flow field, the confinement effect appears negligible, so the prefactors $f_1$ and $f_2$ barely depend on the confinement length scale. Based on the above arguments, the simplified forms of the prefactors
\begin{align}
&f_1=\frac{40(\hat{\mu}+1)}{(2 \hat{\mu}+3)(19 \hat{\mu}+16)}\\
&f_2=\frac{5}{2 \hat{\mu}+3}
\end{align}

\noindent are chosen for this work, where they depend solely on the viscosity ratio $\hat{\mu}=\mu_p / \mu_c$ ($p$ for particle and $c$ for carrier), which is set to 0.01 in this work, in line with the study of Spadan et al. [].

\section{Governing equations of bubble motion}

\noindent The motion equation of the deformable ellipsoidal bubble is based on the formulation of Yin et al. [] and Njobuenwu et al. [] for rigid particles, which has been adopted by Spandan et al. in their study of deformable sub-Kolmogorov bubbles in Taylor Cuoette flow []. According to Spandan et al., several conditions are to be fulfilled so that the move function for rigid particles can be applied to the case of deformable bubbles. Firstly, the bubble interface is fully contaminated with impurities or surfactants which means a no-slip interfacial boundary condition, similar to that of the rigid particles []. Secondly, the characteristic flow time scale is much larger than the interfacial relaxation time scale. Thirdly, the ellipsoidal particles are axisymmetric, which is the case in the work of Yin et al. [] and Njobuenwu et al. []. Under these conditions, the capillary number $Ca$ of the simulation is limited to a maximum of 0.1, and the momentum equation for the dispersed phase can be written as
\begin{align}
\begin{aligned}
\rho_p V_p \frac{d \boldsymbol{v}}{d t}= & 0.5 \rho A_D C_D|\boldsymbol{u}-\boldsymbol{v}|(\boldsymbol{u}-\boldsymbol{v})+0.5 \rho A_L C_L \frac{\boldsymbol{\hat{e}_3}(\boldsymbol{u}-\boldsymbol{v})}{|\boldsymbol{u}-\boldsymbol{v}|}\left(\boldsymbol{\hat{e}_3} \times(\boldsymbol{u}-\boldsymbol{v})\right) \times(\boldsymbol{u}-\boldsymbol{v}) \\
& +\rho V_p C_A\left(\frac{D \boldsymbol{u}}{D t}-\frac{d \boldsymbol{v}}{d t}\right)+\rho V_p\left(\frac{D \boldsymbol{u}}{D t}-\boldsymbol{g}\right)+\rho_p V_p \boldsymbol{g}.
\label{eq::motion_eq}
\end{aligned}
\end{align}

\noindent The first term on the right-hand side of equation (2.1) corresponds to the drag force, the second term corresponds to the lift force, while the last two terms represent the added-mass force and buoyancy, respectively. $\boldsymbol{u}$ and $\boldsymbol{v}$ represent the velocities of the fluid and bubble, respectively. $\frac{D}{D t}$ is the material derivative, $\frac{d}{d t}$ is the derivative along the bubble trajectory, $\boldsymbol{\hat{e}_3}$ is the direction of the bubble major axis, $\boldsymbol{g}$ is the gravitational acceleration, $V_p$ is the volume of the bubble, and the densities of the dispersed phase and the carrier phase are $\rho_p$ and $\rho$, respectively. The drag coefficient $C_D$ in the work of Spandan et al. [] is founded on the approach developed by Ganser [], which combines the drag experienced in the Stokes regime (a linear relationship between drag and velocity) and in the Newtonian regime (a quadratic relationship between drag and velocity). The lift coefficient $C_L$ corresponds to the formulation suggested by Hoerner [], which assumes that the lift is directly proportional to the drag force and is dependent on the relative orientation of the ellipsoid major axis with the slip velocity. The symbols $A_D$ and $A_L$ represent the projected areas normal to the direction of drag and lift, respectively. These values can be computed by the following correlations
\begin{align}
\label{eq::C_D}
&\frac{C_D}{K_2}=\frac{24}{Re_p K_1 K_2}\left(1+0.118\left(Re_p K_1 K_2\right)^{0.6567}\right)+\frac{0.4305}{1+\frac{3305}{Re_p K_1 K_2}}\\
&C_L=C_D \sin ^2 \alpha \cos \alpha\\
&K_1=\frac{1}{3} d_n /(2 R)+\frac{2}{3} \psi{ }^{-0.5}\\
&K_2=10^{1.8148(-\log \psi)^{0.5743}}\\
\label{eq::A_D}
&A_D=\pi R^2\left(\cos ^2 \alpha+\left(4 d_{31} / \pi\right)^2 \sin ^2 \alpha\right)^{1 / 2}\\
&A_L=\pi R^2\left(\sin ^2 \alpha+\left(4 d_{31} / \pi\right)^2 \cos ^2 \alpha\right)^{1 / 2}.
\end{align}

\noindent $Re_p=2 R|\boldsymbol{u}-\boldsymbol{v}| / \nu$ is the particle Reynolds number calculated based on the initial undistorted radius $R$ of the bubble. $\nu$ represents the kinematic viscosity of the carrier flow. Coefficients $K_1$ and $K_2$ are used to consider the bubble sphericity $\psi$, which represents the ratio of the surface area of a deformed bubble to the surface area of an undeformed one. $\alpha$ represents the angle between the largest semiaxes and the relative slip velocity ($\boldsymbol{u}$ - $\boldsymbol{v}$), and $d_{31}$ = $d_3$/$d_1$ represents the ratio between the largest ($d_3$) and smallest ($d_1$) semiaxes. $d_n=\left(4 A_D / \pi\right)^{1 / 2}$ is the diameter of a sphere with the same projection area as that of the deformed bubble, $A_D$.

\noindent By computing the added mass coefficient $C_A$, the formulation of Lai and Mockros for axisymmetric ellipsoidal bubbles is adopted [], which is founded on the ratio of the semiaxes $d_{31}$. According to Spandan et al. [], the simplification of the added mass coefficient for a triaxial ellipsoid from a second-order tensor to a fully isotropic coefficient is due to the complex and intractable nature of the problem given. The correlation reads
\begin{align}
C_A=\frac{d_{31} \ln \left(d_{31}+\sqrt{d_{31}^2-1}\right)-\sqrt{d_{31}^2-1}}{d_{31}^2 \sqrt{d_{31}^2-1}-d_{31} \ln \left(d_{31}+\sqrt{d_{31}^2-1}\right)}.
\label{eq::c_a}
\end{align}

%: ----------------------- HELP: latex document organization
% The commands below help you to subdivide and organize your thesis
%    \chapter{}       = level 1, top level
%    \section{}       = level 2
%    \subsection{}    = level 3
%    \subsubsection{} = level 4
% Note that everything after the percentage sign is hidden from the output
% ----------------------------------------------------------------------