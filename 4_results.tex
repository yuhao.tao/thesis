
\chapter{Results}

\section{Shape evolution}

\subsection{Relaxation}

\noindent In the relaxation test case for the bubble shape function, the temporal evolution of a single bubble with initial aspect ratio $d_{31,0}=4$ is investigated, which is influenced only by surface tension under a constant Kolmogorov time scale $\tau_\eta$ of 1000 in LB frame. The strain rate and vorticity tensor in Eq. (\ref{eq::shape_func_nondim}) are set to zero, and the capillary number $Ca=0.01,0.03,$ and $0.1$ are chosen for the test case, which is in the range of interest. The motion equation is turned off.
\begin{figure}
  \centering
\includegraphics[width=0.7\textwidth]{figures/rx_ar.pdf}  
  \caption{Quasi-exponential decay of the aspect ratio for a bubble with $Ca=0.01,0.03,$ and $0.1$. The time is relative to the Kolmogorov time scale.}
  \label{fig::rx_ar}
\end{figure}

\noindent The simulation reproduces the characteristic behavior of a deformed bubble recovering to the spherical form under the influence of surface tension. In Fig. \ref{fig::rx_ar}, the quasi-exponential decay of the bubble aspect ratio to 1 can be observed, and a smaller $Ca$ increases the rate of the decay. This is reasonable because of the $Ca=\tau/\tau_\eta$. With constant $\tau_\eta$, smaller $Ca$ corresponds to smaller bubble interfacial relaxation time $\tau$, which represents a quicker recovery of the bubble to the spherical form.

\noindent Since the shearing and rotational influences are neglected for this test case, the ordinary differential equation Eq. (\ref{eq::shape_func_nondim}) is simplified to
\begin{align}
\frac{d \boldsymbol{S}^*}{d t^*}=-\frac{f_1}{Ca}\left(\boldsymbol{S}^*-g\left(\boldsymbol{S}^*\right) \boldsymbol{I}\right), 
\label{eq::shape_func_relax}
\end{align}

\noindent where $Ca/f_1$ serves as the theoretical time constant $\tau'_{theoretical}$ of the exponential decay for Fig. \ref{fig::rx_tau_t} and Fig. \ref{fig::rx_tau_ca}. In the relaxation case, the shape function matrix maintains a diagonal form, and the three diagonal values represent the eigenvalues of the matrix, which correlate with the length of the three semi-principal axes. Their respective time constant $\tau'$ in \ref{fig::rx_tau_t} for exponential decay can be calculated with the simulation output of the shape function according to
\begin{align}
\tau'_i=\frac{1-\lambda_{i,n}}{\lambda_{i,n+1}-\lambda_{i,n}}\Delta t,
\end{align}

\noindent where $\lambda_{i,n}$ is an eigenvalue of shape matrix at this time step, $\lambda_{i,n+1}$ is an eigenvalue at the next time step and $\Delta t$ is the time increment. As demonstrated in Fig. \ref{fig::rx_tau_t}, $\tau'$ is not temporally constant either for the semi-major or the semi-minor axis, which is due to the volume conservation function $g\left(\boldsymbol{S}^*\right)$. The function is observed to rise from a positive value larger than the initial length of the semi-minor axis to 1 during the relaxation, which indicates a changing steady state of Eq. (\ref{eq::shape_func_relax}). Hence, the process behavior deviates greatly from purely exponential. Since $g\left(\boldsymbol{S}^*\right)<1$, the decay of the semi-major axis appears quicker than the exponential case, as the mathematical steady-state $g\left(\boldsymbol{S}^*\right)$ is farther than 1, corresponding to a smaller time constant $\tau'$ than the theoretical value $\tau'_{theoretical}$. On the contrary, the growth of the semi-minor axis is slower than the exponential case, as the mathematical steady state is closer than 1, corresponding to larger $\tau'$ than $\tau'_{theoretical}$, matching the results in Fig. \ref{fig::rx_tau_t}. 
\begin{figure}
       \begin{minipage}[t]{0.5\linewidth}
		\centering
		\includegraphics[width=2.8in]{figures/rx_tau_t.pdf}
		\caption{Temporal evolution of the time constants $\tau'$ during the quasi-exponential decay with $Ca = 0.1$, comparing with the theoretical value from the ODE setting. The time is relative to the Kolmogorov time scale.}
		\label{fig::rx_tau_t}
	\end{minipage}
	\begin{minipage}[t]{0.5\linewidth}
		\centering
		\includegraphics[width=2.8in]{figures/rx_tau_ca.pdf}
		\caption{Comparison of the average time constants $\bar{\tau'}$ for the quasi-exponential decay of the shape function tensor, between the calculated values of the semi-major and semi-minor axes of shape function tensor and the constant theoretical value from the ODE setting.}
		\label{fig::rx_tau_ca}
	\end{minipage}
\end{figure}

\noindent Besides, it was observed that the function $g\left(\boldsymbol{S}^*\right)$ approximates 1 and achieves a value above 0.99 after the simulation time of $3\tau$, where the behavior of ODE becomes nearly exponential. This corresponds to the trend of convergence of $\tau'_{semi-major}$ and $\tau'_{semi-minor}$ to $\tau'_{theoretical}$ in Fig. \ref{fig::rx_tau_t}. To characterize the non-exponential nature of Eq. (\ref{eq::shape_func_relax}), the average time constants $\bar{\tau'}$ before $3\tau$ and their standard deviations are calculated for the cases with $Ca=0.01,0.03,$ and $0.1$, illustrated in Fig. \ref{fig::rx_tau_ca}. The average time constants are in a linear correlation with $Ca$, in line with $\tau'_{theoretical}=Ca/f_1$. And the results of the semi-major axis, the theoretical value, and that of the semi-minor axis range from small to large for the same $Ca$, in line with the deduction in the last paragraph.

\noindent The correction of rotation-induced geometrical error hibernates as no vorticity is present, and the forced volume control thoroughly nullifies the volume change due to discretization error. The effect of the volume control on the bubble dynamics is negligible, as it is about four orders of magnitude smaller than the effect of changing the integration strategy from a simple Euler to a predictor-corrector scheme. 

\subsection{Shear flow}

\begin{figure}
  \centering
\includegraphics[width=0.9\textwidth]{figures/shear_channel.pdf}  
  \caption{Schematic of bubble deformation in the shear flow, $\theta$ for orientation of the bubble semi-major axis}
  \label{fig::shear_channel}
\end{figure}

\noindent In the test case for the shear flow, a similar configuration to the work of Maffettone and Minale is employed []. The motion equation is turned off. The surface tension term is enabled, and the strain rate tensor $\boldsymbol{E}^*$ and vorticity tensor $\boldsymbol{\Omega}^*$ in Eq. (\ref{eq::shape_func_nondim}) are set to constant values of 
\begin{equation}
\boldsymbol{E}^*=\frac{1}{2}\left(\begin{array}{lll}
0 & 1 & 0 \\
1 & 0 & 0 \\
0 & 0 & 0
\end{array}\right) , \quad \boldsymbol{\Omega}^*=\frac{1}{2}\left(\begin{array}{ccc}
0 & 1 & 0 \\
-1 & 0 & 0 \\
0 & 0 & 0
\end{array}\right).
\end{equation}

\noindent The test case simulates an initially spherical bubble, which is deformed in the shear channel, as shown in the schematic in Fig. \ref{fig::shear_channel}. The capillary number $Ca$ is set from $10^{-6}$ to 0.6 to validate the dynamics of the shape function by comparing it with the analytical solution [] and experimental data [][][]. The Kolmogorov time scale is set constant and tuned according to $Ca$ to make sure the interfacial relaxation can be resolved. 

\noindent All the test cases with interfacial relaxation time $\tau>1$ exhibit a steady state, where the bubble orientation $\theta$ and aspect ratio $d_{31}$ are a perfect match with the theoretical values according to Maffettone and Minale [],
\begin{align}
&d_{31,\infty}=\sqrt{\frac{f_1^2+Ca^2+f_2 Ca \sqrt{f_1^2+Ca^2}}{f_1^2+Ca^2-f_2 Ca \sqrt{f_1^2+Ca^2}}},\\
&\theta_{\infty}=\frac{1}{2} \arctan \left(\frac{f_1}{Ca}\right)
\end{align}

\noindent which validates the effectiveness of the implementation on the mentioned parameter range. Physically, the steady states of $\theta$ and $d_{31}$ are a pair of standoffs. As the strain rate tensor tends to shear the bubble geometry into more and more slender form, the relaxation term pulls the bubble back to a sphere due to the surface tension, where $d_{31,\infty}$ comes to exist. While the vorticity tensor tends to rotate the bubble, the strain rate tensor seeks to fix the bubble orientation at 45 degrees, and the two forces reach a balance point at $\theta_{\infty}$.
\begin{figure}
  \centering
\includegraphics[width=0.7\textwidth]{figures/theta_ca.pdf}  
  \caption{Correlation of steady orientation of the bubble semi-major axis in the shear flow with the capillary number, comparison with empirical models and analytical solution}
  \label{fig::theta_ca}
\end{figure}

\noindent In Fig. \ref{fig::theta_ca}, $\theta_{\infty}$ in $Ca \geq 0.05$ cases are compared with the theoretical value [], model of Hinch et al. [] for small $Ca$ and model of Rallison [] for large $Ca$, which were experimentally validated by Rust et al. [] using a variation of "parallel band" device for the generation of shear flow. The steady-state values are in perfect match with the theoretical calculation regardless of the variation of Kolmogorov time scale $\tau_\eta$, which represents the variation of the solution time step. In $Ca<0.5$ cases, the model yields results in line with the prediction of Hinch et al., while $Ca \geq 0.5$ cases better match Rallison's model. Besides, a critical capillary number according to Maffettone and Minale []
\begin{equation}
Ca_{cr}=\frac{f_1}{\sqrt{f_2^2-1}}
\end{equation}

\noindent corresponds to the drop rupture, which is also reproduced in the simulation and indicated by the red line in Fig. \ref{fig::theta_ca}.
\begin{figure}
  \centering
\includegraphics[width=0.7\textwidth]{figures/theta_e.pdf}  
  \caption{The temporal evolution of the rotation angle of a bubble with $Ca=0.5$ under the different intensity of straining $\alpha$. Time is non-dimensional.}
  \label{fig::theta_e}
\end{figure}

\noindent For more general cases, where the strain rate tensor and the vorticity tensor read
\begin{equation}
\boldsymbol{E}^*=\left(\begin{array}{lll}
0 & \alpha & 0 \\
\alpha & 0 & 0 \\
0 & 0 & 0
\end{array}\right) , \quad \boldsymbol{\Omega}^*=\left(\begin{array}{ccc}
0 & \beta & 0 \\
-\beta & 0 & 0 \\
0 & 0 & 0
\end{array}\right).
\end{equation}

\noindent where $\alpha$ and $\beta$ can be understood as the intensity of straining and vorticity, respectively. The steady-state formulas in the work of Maffettone and Minale [] can be extended to
\begin{align}
&d_{31,\infty}=\sqrt{\frac{f_1^2/Ca^2+4\beta^2+2\alpha f_2 \sqrt{f_1^2/Ca^2+4\beta^2}}{f_1^2/Ca^2+4\beta^2-2\alpha f_2 \sqrt{f_1^2/Ca^2+4\beta^2}}},\\
\label{eq::std_theta}
&\theta_{\infty}=\frac{1}{2} \arctan \left(\frac{f_1}{2\beta Ca}\right).
\end{align}

\noindent With these correlations, the transition from pure bubble rotation to being attracted by the steady-state point can be studied. The initial bubble aspect ratio $d_{31,0}$ is set to 4, $\beta$ is fixed to 0.5, $\tau_\eta$ in LB frame is set to 100, and $\alpha$ is varied at a range close to zero. It's indicated in Fig. \ref{fig::theta_e} that the bubble demonstrates linear rotational behavior at the initial stage in all three cases. This is because the bubble aspect ratio $d_{31}$ at the initial transience is so large, that the vorticity term dominates the bubble motion. With decreasing $d_{31}$ due to the surface tension term, the bubble deviates more and more from the linear rotational behavior and tends to be attracted by a local steady state. Near the steady state, an acceleration toward the value is observed, where the straining term is considered to be dominant. The final steady state is a balance between the three forces, namely straining, rotating, and surface tension. The straining force pulls the bubble orientation to $\pi/4$ and tends to increase $d_{31}$ to an infinite value, which is compensated by the rotating force, which tries to rotate the bubble further, and surface tension force, which keeps reducing $d_{31}$ to 1, respectively. The calculated theoretical value with Eq. (\ref{eq::std_theta}) is a good match with the simulation employing $\alpha=0.05$. It is marked in Fig. \ref{fig::theta_e} that the steady-state values have the period of $\pi$, which is reasonable considering the axisymmetry of the bubble.

\subsection{Vortex}

\noindent The test case for the vortex reproduces the rotational bubble motion without deformation. The initial bubble aspect ratio $d_{31,0}$ is set to 4, $\tau_\eta$ is varied from 20 to 100, the surface tension is turned off, the strain rate tensor $\boldsymbol{E}^*$ in Eq. (\ref{eq::shape_func_nondim}) is set to zero, and the vorticity tensor $\boldsymbol{\Omega}^*$ is set to a constant value of
\begin{equation}
\boldsymbol{\Omega}^*=\frac{1}{2}\left(\begin{array}{ccc}
0 & 1 & 0 \\
-1 & 0 & 0 \\
0 & 0 & 0
\end{array}\right).
\end{equation}

\begin{figure}
       \begin{minipage}[t]{0.5\linewidth}
		\centering
		\includegraphics[width=2.8in]{figures/vort_theta.pdf}
		\caption{}
		\label{fig::vort_theta}
	\end{minipage}
	\begin{minipage}[t]{0.5\linewidth}
		\centering
		\includegraphics[width=2.8in]{figures/vort_omega.pdf}
		\caption{}
		\label{fig::vort_omega}
	\end{minipage}
\end{figure}

\noindent As demonstrated in Fig. \ref{fig::vort_theta} and Fig. \ref{fig::vort_omega}, linear rotational motion is observed, reflecting the constant vorticity tensor. The angular velocity relative to the time step is in an inverse linear correlation with $\tau_\eta$, the Kolmogorov time scale in the LB frame. This is because the time step of the shape function in the LPT solver is $1/\tau_\eta$, which aims at depicting the fact that a more violent flow field yields smaller $\tau_\eta$ so that the temporal evolution of the bubble geometry in LPT frame is correspondingly quicker. Moreover, the rotation-induced geometrical error is observed to be nullified with the introduced extra term in Eq. (\ref{eq::shape_corr}). 

\section{Rising bubble}

\noindent The simulation of a single rising bubble is conducted to validate the implementation of the motion equation Eq. ($\ref{eq::motion_eq}$) and investigate the influence of the bubble deformability on this process. A cubic simulation box with the side length of $\pi/5~m$ and periodic boundary condition is chosen. An initially spherical bubble with a diameter $d_p$ = $2 \cdot 10^{-3}$ m is set free from $x_p(t=0)=(0, 0, 0)^{T}m$ and moves in the flow field under the gravitational acceleration of $\boldsymbol{g} = (0, -9.81, 0)^{T} m/s^{2}$. The initial configuration is showcased in Fig. \ref{fig::rising_bubble}. The density of the fluid is 1000 $kg/m^3$ and the density of the bubble is 1 $kg/m^3$, similar to the density of air. The kinematic viscosity $\nu$ of the fluid is set to $10^{-6}~m^2/s$. A uniform refinement of level 7 is applied, corresponding $2^7=128$ cells in each direction. 
\begin{figure}
\begin{center}
  \begin{tikzpicture}[x={(0.8cm, 0cm)}, y={(0cm,0.8cm)}, z={(0.2832cm,0.2832cm)}]\
    \draw [->,>=stealth,line width=1pt] (3.5,0,3.5) -- (3.5,0,2.0) node [left]{$z$};
    \draw [->,>=stealth,line width=1pt] (3.5,0,3.5) -- (3.5,1.5,3.5) node [above]{$y$};
    \draw [->,>=stealth,line width=1pt] (3.5,0,3.5) -- (5.0,0,3.5) node [right]{$x$};
    \draw [<->,>=stealth,line width=0.5pt] (0,-0.25,0) -- (7,-0.25,0) node [pos=0.5,below=1.5]{$L = \pi/5$};
    \draw [->,>=stealth,line width=1pt] (-0.5,7,0) -- (-0.5,5.5,0) node [pos=0.5,left]{$\boldsymbol{g}$};
    \shade[ball color = lightgray,opacity = 0.5] (3.5,3.5,3.5) circle (0.5cm);
    \draw [line width=1pt] (0,0,0) -- (7,0,0);
    \draw [line width=1pt] (0,0,0) -- (0,7,0);
    \draw [line width=1pt] (0,0,0) -- (0,0,7);
    \draw [line width=1pt] (7,0,0) -- (7,7,0);
    \draw [line width=1pt] (7,0,0) -- (7,0,7);
    \draw [line width=1pt] (0,7,0) -- (7,7,0);
    \draw [line width=1pt] (0,7,0) -- (0,7,7);
    \draw [line width=1pt] (0,0,7) -- (7,0,7);
    \draw [line width=1pt] (0,0,7) -- (0,7,7);
    \draw [line width=1pt] (7,7,0) -- (7,7,7);
    \draw [line width=1pt] (7,0,7) -- (7,7,7);
    \draw [line width=1pt] (0,7,7) -- (7,7,7);
  \end{tikzpicture}
  \caption{Domain for the single rising bubble setup with periodic BC at all domain boundaries}
  \label{fig::rising_bubble}
\end{center}
\end{figure}

\noindent While $Ma^*=0.1$ and $Ca\in[0, 0.1]$ are adopted, $Re$ and $Fr$ are chosen according to the terminal velocity of the bubble, which can be analytically determined. Under the assumption that drag and buoyancy are the dominant influences on the bubble motion, which is demonstrated in Fig. \ref{fig::rb_mag}, Eq. \ref{eq::motion_eq} is simplified to
\begin{align}
\begin{aligned}
0.5 \rho A_D C_D|\boldsymbol{u}-\boldsymbol{v}|(\boldsymbol{u}-\boldsymbol{v})=(\rho-\rho_p)V_p\boldsymbol{g}
\end{aligned}
\end{align}

\noindent at the steady state. Moreover, the flow field is hardly agitated by the bubble in similar test cases. It is hence further assumed that the flow velocity is negligible and the direction of the bubble velocity is approximately on the contrary of the gravity. The terminal velocity $v_T$ then has the form 
\begin{align}
\begin{aligned}
v_T=\sqrt{\frac{2(\rho-\rho_p)V_pg}{\rho A_D C_D}}.
\label{eq::v_T}
\end{aligned}
\end{align}

\noindent With negligible flow field, the shape function of the bubble (Eq. (\ref{eq::shape_function})) is barely triggered, so that the bubble deformation is considered to be trivial, which leads to another assumption that the bubble maintains a spherical form during the process, namely $d_{31}=\psi=K_2=1$ for the motion equation Eq. (\ref{eq::motion_eq}) and its sub-equations. Without the dependency on the aspect ratio $d_{31}$, the projected area $A_D(\alpha)$ vibrates in a small interval of $[\pi R^2, 4R^2]$ with a periode of $\pi$. Considering $A_D = 4R^2$, the coefficient $K_1 = 2/(3\sqrt{\pi})+2/3$. With $Re_p=d_pv_T/\nu=2000v_T$, 
\begin{align}
\begin{aligned}
C_D(v_T)=\frac{1+0.118(2085.586v_T)^{0.6567}}{86.8994v_T}+\frac{0.4305}{1+\frac{1.584686}{v_T}}.
\end{aligned}
\end{align}

\noindent Substituting the values into Eq. (\ref{eq::v_T}), then the formula becomes $v_T=f(v_T)$, and yields the result of $v_T=0.21248m/s$. The $Re_p$ and $Fr_p$ for this simulation are consequently 424.96 and 1.517, respectively.
\begin{figure}
  \centering
\includegraphics[width=0.7\textwidth]{figures/rb_couple.pdf}  
  \caption{Comparison of the temporal evolution of the bubble velocity magnitude $|v|$ with different coupling methods. The abrupt change of the bubble velocity magnitude in the two-way coupled case and the velocity magnitude difference between one-way and two-way coupled cases are highlighted.}
  \label{fig::rb_couple}
\end{figure}

\noindent In Fig. \ref{fig::rb_couple}, the temporal evolution of the bubble velocity magnitude is compared with the analytical terminal velocity, and the results with one-way and two-way coupling are juxtaposed. The simulation result is generally a good match with the analytical solution. The two-way coupling itself is observed to slightly enhance the bubble velocity, shown by the blue line, which is in line with the result of Grafen []. It is because the motion of a spherical particle is less impeded in a mobile flow field, namely the two-way coupled case, where the moving fluid with the bubble leads to a smaller velocity difference between the particle and the fluid, hence lower drag. However, the velocity increase is one to two orders of magnitude smaller than the results of Grafen. This is because the particle density is 2560 times smaller than in his case, so the momentum transport to the fluid due to the bubble motion is much weaker, causing a much less agitated flow field.  
\begin{figure}
  \centering
\includegraphics[width=0.7\textwidth]{figures/rb_ca.pdf}  
  \caption{Comparison of the temporal evolution of the bubble velocity magnitude $|v|$ with different capillary number $Ca$. Terminal velocity is evidenced in small $Ca$ cases.}
  \label{fig::rb_ca}
\end{figure}

\noindent In Fig. \ref{fig::rb_ca}, an upsurge of the bubble velocity magnitude is noticed before 0.05 seconds. The relatively high levels of lift term and added mass term at this stage are considered to contribute to this phenomenon according to Fig. \ref{fig::rb_mag}. Moreover, the bubble velocities in the two-way coupled cases are observed to decrease until a certain steady value, which can be lower than the analytical terminal velocity, demonstrated in Fig. \ref{fig::rb_ca}.  This velocity decline is considered to be the joint effort of the bubble deformation and rotation, consequently increasing the drag effect. Besides, a sudden acceleration of the bubble can be observed at approximately 0.34 seconds, highlighted in Fig. \ref{fig::rb_couple}. The reason is the switch of the bubble semi-major axis, which can be visualized by the data in Fig. \ref{fig::rb_shape}. Consequently, the lift term is reduced to zero because the new semi-major axis is observed to be perpendicular to the velocity difference vector $(\boldsymbol{u}-\boldsymbol{v})$ between the flow field and the bubble, demonstrated in Fig. \ref{fig::rb_mag}. As shown in Fig. \ref{fig::rb_ca}, the larger the capillary number $Ca$, the longer the process of velocity decrease. To better understand this transient state, the case with $Ca=0.1$ is further probed. 
\begin{figure}
  \centering
\includegraphics[width=0.7\textwidth]{figures/rb_mag.pdf}  
  \caption{The magnitude of the acceleration terms in motion equation (Eq. \ref{eq::motion_eq}) relative to the magnitude of the constant buoyancy term on a logarithmic scale for $Ca=0.1$. The red cross highlights the abrupt change of the lift term.}
  \label{fig::rb_mag}
\end{figure}

\noindent In Fig. \ref{fig::rb_mag}, the magnitude of the temporal evolution of drag, lift, added mass, and buoyancy terms are juxtaposed on a logarithmic scale. The drag and buoyancy are dominant after 0.05 seconds, which may justify the assumption drawn for calculating the bubble terminal velocity. The lift term decreases steadily after the initial transience with the bubble semi-major axis rotating towards the direction perpendicular to the velocity difference $(\boldsymbol{u}-\boldsymbol{v})$, until the sudden change of the bubble semi-major axis.
\begin{figure}
  \centering
\includegraphics[width=0.7\textwidth]{figures/rb_drag.pdf}  
  \caption{Temporal evolution of the drag factor $C_D$, projected area normal to the direction of drag $A_D$, and bubble aspect ratio $d_{31}$ relative to their respective reasonable initial value with $Ca=0.1$ and $10^{-4}$}
  \label{fig::rb_drag}
\end{figure}

\noindent In Fig. \ref{fig::rb_drag}, the evolutions of three drag-related factors, namely the projected area $A_D$, drag factor $C_D$, and bubble aspect ratio $d_{31}$ in the two-way coupled case are demonstrated. The bubble-induced flow field causes the bubble geometry to deviate from the perfect sphere. The increase of $A_D$ and $C_D$ contributes directly to the drag in Eq. (\ref{eq::motion_eq}), reducing the velocity difference $(\boldsymbol{u}-\boldsymbol{v})$ to compensate the constant buoyancy term, hence indirectly lowers the bubble velocity. The increase of $A_D$ is a synergy of bubble rotation and deformation. With the rotation, the angle $\alpha$ between the bubble semi-major axis and the velocity difference $(\boldsymbol{u}-\boldsymbol{v})$ is observed to approximate $\pi/2$, where the expression in Eq.(\ref{eq::A_D}) achieves the maximum with fixed bubble radius $R$ and aspect ratio $d_{31}$. And the deformation, namely the rising $d_{31}$, ensures the steady increase of $A_D$ beyond the maximum. Decreasing $(\boldsymbol{u}-\boldsymbol{v})$ due to the growing $A_D$ then lowers the particle Reynold number $Re_p=2 R|\boldsymbol{u}-\boldsymbol{v}| / \nu$, magnifying $C_D$ according to Eq. (\ref{eq::C_D}). Besides, the drag-related factors showcase a steady state for $Ca=10^{-4}$, which is in line with its temporal velocity profile in Fig. \ref{fig::rb_couple}.
\begin{figure}
  \centering
\includegraphics[width=0.7\textwidth]{figures/rb_shape.pdf}  
  \caption{Temporal evolutions of the non-dimensional lengths of bubble semi-principal axes and the angle between the semi-principal axes and the velocity difference vector $(\boldsymbol{u}-\boldsymbol{v})$ with $Ca=0.1$. $\alpha$ and $d_3$ for semi-major axis, $\gamma$ and $d_1$ for semi-minor axis, and $\beta$ and $d_2$ for the third semi-principal axis.}
  \label{fig::rb_shape}
\end{figure}

\noindent Besides, an oblate geometry of the bubble, which is consistent with the experimental observation of Rosenberg \cite{Rosenberg1950} and study of Saffman \cite{Saffman1956}, and its tendency to get more oblate is observed throughout the simulation, with the semi-minor axis pointing approximately toward the direction of the bubble motion, as shown in Fig. \ref{fig::rb_shape}. This strategy maximizes the projected area normal to the direction of drag. 

\noindent These mechanisms together may account for the velocity decrease of the bubble observed in Fig. \ref{fig::rb_couple} in the two-way coupled case with $Ca=0.1$, and the general velocity decrease before the steady state of deformable bubbles.

\section{Decaying isotropic turbulence}



































%\begin{figure}
%  \centering
%  % if you want to include the text code produced
%  % By the Gnuplot lua tikz terminal you have to
%  % uncomment \usepackage{gnuplot-lua-tikz} in the
%  % main file first
%  % \input{figures/rst_tikz.tex}
%
%  % or you can include the latex + eps file generated
%  % by the epslatex terminal from gnuplot (you have to add
%  % the 'figures' folder to the path to the eps file in the
%  % input tex file)
%  % \input{figures/rst_epslatex.tex}
%
%  % the other option is to include the standalone pdf
%  \includegraphics[width=0.5\textwidth]{figures/rst_tikz.pdf}  
%  \caption{Example of a line plot included from PDF}
%  \label{fig::rst_tikz}
%
%\end{figure}
%
%\begin{figure}
%  \centering
%  \input{figures/rst_epslatex.tex}
%  \caption{Example of a line plot included using epslatex}
%  \label{fig::rst_epslatex}
%
%\end{figure}

