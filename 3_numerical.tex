\chapter{Numerical Methods}

\section{The lattice Boltzmann method}

\subsection {Discretization}

\noindent In LBM, the distribution function $f(\boldsymbol{\xi},\boldsymbol{x},t)$ is discretized in velocity, space and time. The discretization in velocity space is achieved by representing the distribution function using a set of discrete velocity vectors, denoted as $\boldsymbol{c}_i$. These vectors are chosen to be the velocities of particles that propagate on a lattice in discrete time steps.

\noindent The distribution function is then approximated by a discrete set of values, $f_i(\boldsymbol{x},t)$, at the lattice points x and time t. The values of the distribution function $f_i(\boldsymbol{x},t)$ represent the density of particles with velocity $\boldsymbol{c}_i$ at position $\boldsymbol{x}$ and time $t$.

\noindent The time evolution of the distribution function is governed by the discrete Boltzmann equation
\begin{equation}
f_i\left(\boldsymbol{x}+\boldsymbol{c}_i \Delta t, t+\Delta t\right)=f_i(\boldsymbol{x}, t)+\Omega_i(\boldsymbol{x}, t)+S_i .
\label{eq::discrete_boltzmann}
\end{equation}

\noindent where $\Delta t$ is the time step, $S_i$ is a source term that accounts for the reaction force of the dispersed phase on the flow, and $\Omega_i$ is the collision operator, typically based on the BGK approximation
\begin{equation}
\Omega_i(f)=-\frac{f_i-f_i^{eq}}{\tau} \Delta t .
\end{equation}

\noindent where $\tau$ is the relaxation time, $f_i$ is the discrete distribution function at velocity $\boldsymbol{c}_i$, and $f_i^{eq}$ is the local equilibrium distribution function at velocity $\boldsymbol{c}_i$. The local equilibrium distribution function is given by the Maxwell-Boltzmann distribution and truncated to polynomial form using Hermite expansion, shown in Eq. (\ref{eq::hermite_equilibrium}).
\begin{equation}
f_i^{eq}(\boldsymbol{x}, t)=w_i \rho\left(1+\frac{\boldsymbol{u} \cdot \boldsymbol{c}_i}{c_{s}^2}+\frac{\left(\boldsymbol{u} \cdot \boldsymbol{c}_i\right)^2}{2 c_{s}^4}-\frac{\boldsymbol{u} \cdot \boldsymbol{u}}{2 c_{s}^2}\right)
\label{eq::hermite_equilibrium}
\end{equation}

\noindent where $\rho$ is the density, $\boldsymbol{u}$ is the macroscopic velocity, $w_i$ is the weight factor, $c_s$ is the speed of sound, and · denotes the dot product. In the implementation, Eq. (\ref{eq::discrete_boltzmann}) is divided into the collision step and streaming step for more efficient computations (Eq. \ref{eq::collision}, Eq. \ref{eq::streaming}). Because the collision step is local and algebraic while the streaming step calls for data on multiple lattice sites [].
\begin{align}
\label{eq::collision}
& f_i^{\star}(\boldsymbol{x}, t)=f_i(\boldsymbol{x}, t)-\frac{\Delta t}{\tau}\left[f_i(\boldsymbol{x}, t)-f_i^{\mathrm{eq}}(\boldsymbol{x}, t)\right] &\quad \text { (collision) } \\
\label{eq::streaming}
& f_i\left(\boldsymbol{x}+\boldsymbol{c}_i \Delta t, t+\Delta t\right)=f_i^{\star}(\boldsymbol{x}, t) &\quad \text { (streaming) }
\end{align}

\noindent The density and momentum, as macroscopic moments, are determined from the $finite~sums$ in the discrete velocity space []
\begin{equation}
\begin{aligned}
\rho & =\sum_i f_i=\sum_i f_i^{eq} \\
\rho \boldsymbol{u} & =\sum_i f_i \boldsymbol{c}_i=\sum_i f_i^{eq} \boldsymbol{c}_i.
\end{aligned}
\end{equation}

\noindent With the discretized velocity space, a velocity set as large as necessary and as small as possible should be chosen for the simulation. While D3Q15, D3Q19, and D3Q27 are potential candidates for three-dimensional cases, D3Q15, and D3Q19 are reported to lack rotational invariance for a few non-linear cases, which can jeopardize the simulation of turbulence []. Therefore, the D3Q27 velocity set is adopted in this work.

\subsection {Non-dimensionalization}

\noindent Non-dimensional variables and parameters are utilized for the calculation, where the reference values are determined as the density $\rho_0$, the mean thermodynamic velocity $\xi_0$, the grid distance $\Delta x$, and the temperature $T_0$. Real quantities such as space and time need to be converted to lattice units before simulation. Non-dimensional quantities remain the same. The following non-dimensional variables are employed in the simulation module
\begin{equation}
\begin{array}{|l|l|}
\hline \multicolumn{1}{|c|}{\text { Variable }} & \multicolumn{1}{c|}{\text { Equation }} \\
\hline \text { Velocity } & u^*=\frac{u}{\xi_0} \\
\hline \text { Isothermal speed of sound } & c_s^*=\frac{c_s}{\xi_0}=\sqrt{\frac{1}{3}} \\
\hline \text { Molecular velocity } & \xi_0^*=\frac{\xi_0}{\xi_0}=1 \\
\hline \text { Grid distance } & \Delta x^*=\frac{\Delta x}{\Delta x}=1 \\
\hline \text { Characteristic Length } & L^*=\frac{L}{\Delta x} \\
\hline \text { Time step } & \Delta t^*=\frac{\Delta t \cdot \xi_0}{\Delta x}=1 \\
\hline \text { Density } & \rho^*=\frac{\rho}{\rho_0} \\
\hline \text { Pressure } & p^*=\frac{p}{\rho_0 \cdot \xi_0^2} \\
\hline \text { Collision Operator } & \Omega^*=\omega \cdot \Delta t=\frac{2}{6 \cdot \nu^*+1} \\
\hline \text { Temperature } & T^*=\frac{T}{T_0} \\
\hline \text { Thermal diffusivity } & \kappa^*=\frac{\nu^*}{P_r} \\
\hline \text { Collision Operator (thermal) } & \Omega_g^*=\omega_g \cdot \Delta t=\frac{2}{6 \cdot \frac{v^*}{Pr}+1} \\
\hline
\end{array}
\end{equation}

\noindent With the Mach number $Ma^*$ and considering Reynolds similarity
\begin{align}
R e=\frac{L \cdot u}{\nu}=\frac{L^* \cdot u^*}{\nu^*}=Re^*,
\end{align}

\noindent the non-dimensional kinematic viscosity $\nu^*$, with which the collision operator is calculated, can be computed by,
\begin{equation}
\nu^*=\frac{u^* \cdot L^*}{R e}=\frac{L}{\sqrt{3} \cdot \Delta x} \cdot \frac{M a^*}{R e}.
\end{equation}

\noindent Besides, the Mach number $Ma^*$ is usually utilized to set the physical time step by
\begin{equation}
M a^*=\frac{u^*}{c_s^*} \Leftrightarrow \Delta t=M a^* \cdot \frac{\Delta x}{\sqrt{3} \cdot u}.
\end{equation}

\noindent Since LBM is based on the kinetic theory that assumes that the fluid is incompressible, $Ma^*$ less than approximately 0.3 is suitable for the simulation. For high $Ma^*$ flows, additional terms can be introduced in the LB equation that accounts for compressibility effects, or the LB solver can cooperate with another solver for high $Ma^*$ by spatial separation of the flow.

\noindent 

\section{Bubble morphology}

\subsection {Non-dimensionalization}

\noindent The shape function is implemented in a non-dimensional form where the dynamics are demonstrated by a control parameter $Ca$, which is fixed during the simulation, similar to the arrangement of Spandan et al. [][][]. The non-dimensional evolution equation is written as
\begin{align}
\frac{d \boldsymbol{S}^*}{d t^*}-\left(\boldsymbol{\Omega}^* \boldsymbol{S}^*-\boldsymbol{S}^* \boldsymbol{\Omega}^*\right)=-\frac{f_1}{Ca}\left(\boldsymbol{S}^*-g\left(\boldsymbol{S}^*\right) \boldsymbol{I}\right)+f_2\left(\boldsymbol{E}^* \boldsymbol{S}^*+\boldsymbol{S}^* \boldsymbol{E}^*\right).
\label{eq::shape_func_nondim}
\end{align}

\noindent Here, $\boldsymbol{S}^*=\boldsymbol{S} / R^2$ is the non-dimensional shape function, where $R$ is the radius of the undeformed spherical bubble. $\boldsymbol{\Omega}^*=\boldsymbol{\Omega} / G$ and $\boldsymbol{E}^*=\boldsymbol{E} / G$ are the non-dimensional vorticity rate and strain rate tensors, where $G=1 / \tau_\eta$ is the inverse of the Kolmogorov turbulent time scale in LPT frame. In this work, $\tau_\eta$ is computed based on the two-point correlation []. $d t^*=d t/\tau_\eta$ is the non-dimensional time step of the shape function, and $Ca=\tau/\tau_\eta$ is the definition of the capillary number.

\subsection {Volume control}

\noindent With the bubble interfacial relaxation time $\tau$ reducing to the level of $O(1)$ in the LB frame, it was observed in the single bubble relaxation test case that the volume change becomes significant. This test case ignores the flow field and aims to examine the process of an ellipsoidal bubble gradually recovering into a sphere under the action of surface tension. Since this process is mathematically proved in the work of Maffettone and Minale to conserve the bubble volume [], the numerical error is considered to harm the integrity of the equation. The predictor-corrector scheme reduces the volume change during relaxation by approximately 1 to 2 orders of magnitude, as shown in Fig. \ref{fig::vol_control}. 
\begin{figure}
  \centering
  \includegraphics[width=0.7\textwidth]{figures/vol_control.pdf}  
  \caption{The correlation of relative volume error with bubble interfacial relaxation time in LB frame for initial aspect ratio $d_{31,0}$ of 2 and 4. Relative volume error is the ratio between the volume change of the bubble due to the relaxation process and the initial volume.}
  \label{fig::vol_control}
\end{figure}

\noindent However, significant volume error at the level of 5\% still exists for the small $\tau$ cases with an initial aspect ratio of 4 and 2, demonstrating the necessity for volume control in these cases. Moreover, since the illustrated data indicate only the volume error production of a single relaxation process, and the large-scale simulations with flow field constantly introduce variations with hardly a steady state, the volume error can accumulate to a large value and harm the integrity of the simulations even for the large $\tau$ cases. With the condition of a low Mach number ($Ma < 0.3$) in this work, which corresponds to the incompressible flow [], an extra step is introduced to control the bubble volume for each time increment, where the volume is set to the initial value.

\subsection {Correction of rotation-induced geometrical error}

\noindent In the vortex test case with a constant vorticity tensor, and without considering surface tension or strain rate, the bubble is expected to perform the pure rotational motion. However, a stepwise change of the bubble geometry is observed, and this change converges to zero with a rising Kolmogorov time scale. Hence, this phenomenon is believed to derive from discretization error. This error is observed to demonstrate no dependency on the direction of the rotation and tends to increase the aspect ratio of the bubble.

\noindent To pursue the decoupling between shearing and rotating motion, an extra term is added in Eq. (\ref{eq::shape_func_nondim}) to compensate for the rotation-induced geometrical error, which is
\begin{align}
&\frac{d \boldsymbol{S}^*}{d t^*}|_{correction}=-\tau_\eta\boldsymbol{V}^*_{n+1}(\boldsymbol{D}^*_{n+1}-\boldsymbol{D}^*_n)\boldsymbol{V}_{n+1}^{*T}, \\
&\boldsymbol{V}^*_{n+1}\boldsymbol{D}^*_{n+1}\boldsymbol{V}_{n+1}^{*T}=\boldsymbol{S}^*_{n+1}=\boldsymbol{S}^*_n+(\boldsymbol{\Omega}_n^* \boldsymbol{S}_n^*-\boldsymbol{S}_n^* \boldsymbol{\Omega}_n^*)d t^*.
\label{eq::shape_corr}
\end{align}

\noindent Here, $\tau_\eta$ is the Kolmogorov time scale in LB frame, $\boldsymbol{S}^*_n$ is the shape function of this time step, $\boldsymbol{S}^*_{n+1}$ is the shape function at a virtual next time step that only considers the vorticity term in Eq. (\ref{eq::shape_func_nondim}), $\boldsymbol{V}^*_{n+1}$ and $\boldsymbol{D}^*_{n+1}$ are the eigenvector and eigenvalue matrices of $\boldsymbol{S}^*_{n+1}$, respectively, and $T$ denotes the transpose of a matrix. This term sets to zero the shape change caused by the vorticity for every time step.

\noindent Besides, it is observed that the correction term introduces a shift of steady orientation in the shear flow test case. As a result, this term is deactivated if the Frobenius norm of the time derivative of the shape tensor is smaller than 0.01, which indicates the proximity of the steady state.

\section{Bubble motion}

\noindent Since a large density ratio between the flow field and bubble leads to instability in the numerical solution of the motion equation, the move function is transformed so that the influence of the density ratio is minimized, which is written as
\begin{align}
\begin{aligned}
\frac{d \boldsymbol{v}}{d t}=&\frac{\rho A_D C_D}{2(\rho_p+\rho C_A) V_p}|\boldsymbol{u}-\boldsymbol{v}|(\boldsymbol{u}-\boldsymbol{v})+\frac{\rho A_L C_L}{2(\rho_p+\rho C_A) V_p} \frac{\boldsymbol{\hat{e}_3}(\boldsymbol{u}-\boldsymbol{v})}{|\boldsymbol{u}-\boldsymbol{v}|}\left(\boldsymbol{\hat{e}_3} \times(\boldsymbol{u}-\boldsymbol{v})\right) \times(\boldsymbol{u}-\boldsymbol{v}) \\
& +\frac{\rho (C_A+1)}{\rho_p + \rho C_A}\frac{D \boldsymbol{u}}{D t}+\frac{\rho_p-\rho}{\rho_p+\rho C_A}\boldsymbol{g}.
\label{eq::motion_eq_stable}
\end{aligned}
\end{align}

\noindent In the calculation of the bubble sphericity $\psi$ for the drag coefficient in the motion equation, the surface area of an ellipsoidal deformed bubble is necessary, where a precise solution is not available. Knud Thomsen's Formula is then utilized to approximate the surface area value, which is reported to yield a relative error of at most 1.061$\%$ []. The formula reads
\begin{equation}
S=4 \pi \sqrt[p]{\frac{a^p b^p+a^p c^p+b^p c^p}{3}}, ~p=1.6075.
\end{equation}

\noindent Here $S$ represents the ellipsoid surface area, and a, b, and c, are the lengths of the three semiaxes.

\noindent Besides, the denominator of Eq. (\ref{eq::c_a}) for the added mass coefficient $C_A$ in the motion equation becomes zero when the bubble is purely spherical, which cripples the numerical solution. Since the $C_A$ is 0.5 for perfect spheres [], the coefficient is set to 0.5 in the implemented module when $d_{31} < 1 + 10^{-6}$. This is in accordance with Eq. (\ref{eq::c_a}), where $C_A(d_{31}=1 + 10^{-6})\approx 0.499994$.

\section{Algorithms}

\subsection{Coupling}

\noindent In the case where the motion of the dispersed phase and the flow field are one-way coupled, only the forces that the fluid exerts on the dispersed phase are considered and the effect of the reaction force of the dispersed phase on the local flow field is ignored. As a result, in the case where the flow field is initially quiescent, namely the rising bubble test case in section 4.2, no geometrical change in the bubble can occur. This means that the bubble geometry is decoupled from the flow field for a one-way coupling between the bubble motion and the fluid. The inclusion of the reaction force of particles on the surrounding flow field, following Newton's third law of motion, is known as the two-way coupling of the bubble motion. This is represented by the source term in Eq. (\ref{eq::discrete_boltzmann}). In this case, the interaction between the bubble and the flow field, as shown in Fig. \ref{fig::corr_network} is enabled, which is appropriate for this study.

\subsection{LPT solution step}

The solution step in the LPT solver for the deformable bubbles can be divided into seven parts. Firstly, the motion equation with stable form (Eq. (\ref{eq::motion_eq_stable})) is solved using the predictor-corrector scheme when the capillary number $Ca \leq 0.1$, which yields data of bubble position, velocity, and acceleration. The morphological change of the bubble is then preliminarily determined by the non-dimensional shape function (Eq. (\ref{eq::shape_func_nondim})) in the predictor-corrector scheme. Next, the rotation-induced shape error is calculated and compensated. Afterward, the bubble volume is set to the initial value, and the data of the bubble shape function is generated. Then, particle exchange between the domains takes place. The redistribution weights are calculated. Finally, the reaction force of the particle on the fluid is determined in the two-way coupled case.


































%\begin{figure}
%\begin{center}
%  \begin{tikzpicture}[scale=1.0]
%  \draw[->,>=stealth] (0,0) -- ++(6.0,0) node [right]{$z$};
%  \draw[->,>=stealth] (0,0) -- ++(0,3.5) node [above]{$y, \eta$};
%    \foreach \i in {0,...,6} {
%      \draw [thick,domain=0:5,variable=\z] (0,0.5*\i) -- plot({\z},{0.5*\i + 0.3*sin((2*3.142*\z/5) r)});
%    }
%    \foreach \i in {0,...,10} {
%      \draw [thick](0+0.5*\i,{0.3*sin((2*3.142*0.5*\i/5) r)}) -- ++ (0,3);
%    }
%    \draw[thick,->,>=stealth] (5,0) -- ++ (0.7,0.28) node [above] {$\zeta$};
%  \end{tikzpicture}
%  \caption{Example of a 2D tikz figure}
%  \label{fig::exampleone}
%\end{center}
%\end{figure}
%
%\begin{figure}
%\begin{center}
%  \begin{tikzpicture}[x={(0.939cm,-0.34cm)}, y={(0cm,1cm)}, z={(0.939cm,0.34cm)}]\
%    \draw [->,>=stealth] (0,0,-1.5) -- (0,0,-2.0) node [left]{$z$};
%    \draw [->,>=stealth] (0,0,-1.5) -- (0,0.5,-1.5) node [above]{$y$};
%    \draw [->,>=stealth] (0,0,-1.5) -- (0.5,0,-1.5) node [right]{$x$};
%    \draw [<->,>=stealth] (0,0,-0.5) -- (7,0,-0.5) node [pos=.5,below=1.0]{$L_x$};
%    \draw [<->,>=stealth] (0,0,-0.5) -- (0,2,-0.5) node [pos=.5,left]{$L_y$};
%    \draw [<->,>=stealth] (7.5,0,0) -- (7.5,0,2) node [pos=.5,below=2.0]{$L_z$};
%
%    \draw [domain=2:5, variable=\x] (0,0,0) -- (1,0,0) sin (1.5,0.05,0) cos (2,0.1,0) -- plot(\x,{0.1*cos((2*3.14159*(\x-2)/3.0) r)}, 0) -- (5,0.1,0) .. controls (5.3,0.1,0) and (5.7,-0.1,0) .. (7,0.0,0) -- (7,2,0) -- (7,2,0) -- (0,2,0) -- cycle;
%
%    \draw [domain=2:5, variable=\x] (0,0,2) -- (1,0,2) sin (1.5,0.05,2) cos (2,0.1,2) -- plot(\x,{0.1*cos((2*3.14159*(\x-2)/3.0) r)}, 2) -- (5,0.1,2) .. controls (5.3,0.1,2) and (5.7,-0.1,2) .. (7,0.0,2) -- (7,2,2) -- (7,2,2) -- (0,2,2) -- cycle;
%
%    \draw[color=red,fill=red, fill opacity=0.2, domain=0:2, variable=\z] plot (2.5,{0.1*sin((1.7*1.5707+2*3.14159*\z) r)},\z)  -- plot[domain=2.5:4.5,variable=\x](\x,{0.1*cos((2*3.14159*(\x-2)/3.0) r)}, 2) --  plot (4.5,{0.1*sin((1.7*1.5707+2*3.14159*\z) r)},2.0-\z)  -- plot[domain=4.5:2.5,variable=\x](\x,{0.1*cos((2*3.14159*(\x-2)/3.0) r)}, 0)  -- cycle;
%
%    \draw (7,0,0) -- (7,0,2);
%    \draw (0,0,0) -- (0,0,2);
%    \draw (0,2,0) -- (0,2,2);
%    \draw (7,2,0) -- (7,2,2);
%    \draw[opacity=0.5, variable=\z, samples at={0,0.05,...,2.05}]
%    plot (5, {0.1*sin((1.5707+2*3.14159*\z) r)}, \z);      
%    \draw[opacity=0.5, variable=\z, domain=0:2]
%    plot (2, {0.1*sin((1.5707+2*3.14159*\z) r)}, \z);
%    \draw [opacity=0.5](2,0.1,2) -- (5,0.1,1);
%    \draw [opacity=0.5](2,0.1,1) -- (5,0.1,0);
%
%    \draw (5,0.1,0) -- (5,0.25,0);
%    \draw (5,0.1,1) -- (5,0.25,1);
%    \draw[<->] (5,0.2,0) -- (5,0.2,1) node [pos=.5,sloped,above] {$\lambda$};
%    
%    \node (x0) at (1.5,0,-1.5) {$x_0$};
%    \draw[->,>=stealth] (x0) -- (1.5,0,-0.6);
%    \node (x0) at (2.5,0,-1.5) {$x_1$};
%    \draw[->,>=stealth] (x0) -- (2.5,0,-0.6);
%    \node (x0) at (4.5,0,-1.5) {$x_2$};
%    \draw[->,>=stealth] (x0) -- (4.5,0,-0.6);
%
%    \node (inflow) at (-1,2,1) {Inflow};
%    % \node (flat) at (1, 0,1) {\small{Wall (flat)}};
%    % \node (flat) at (3.3, 0,1) {\small{Wall (wave)}};
%    \draw[<->,>=stealth] (3,1.8,0) -- (3,1.8,-0.2) -- (3,2.2,-0.2) -- node[pos=.5,sloped,above] {Periodic BC} (3,2.2,2.2) -- (3,1.8,2.2) -- (3,1.8,2.0);
%    \draw[color=blue, dashed] (0,0,1) .. controls (0.5,0.2,1) ..  (0.5,2,1) -- (0,2,1) -- (0,0,1);
%    \draw[color=blue, ->,>=stealth] (0,0.2,1) -- (0.3,0.2,1);
%    \draw[color=blue, ->,>=stealth] (0,0.4,1) -- (0.45,0.4,1);
%    \draw[color=blue, ->,>=stealth] (0,0.6,1) -- (0.5,0.6,1);
%    \draw[color=blue, ->,>=stealth] (0,0.8,1) -- (0.5,0.8,1);
%    \draw[color=blue, ->,>=stealth] (0,1.0,1) -- (0.5,1.0,1);
%    \draw[color=blue, ->,>=stealth] (0,1.2,1) -- (0.5,1.2,1);
%    \draw[color=blue, ->,>=stealth] (0,1.4,1) -- (0.5,1.4,1);
%    \draw[color=blue, ->,>=stealth] (0,1.6,1) -- (0.5,1.6,1);
%    \draw[color=blue, ->,>=stealth] (0,1.8,1) -- (0.5,1.8,1);
%  \end{tikzpicture}
%  \caption{Example of a 3D tikz plot.}
%  \label{fig::grid}
%\end{center}
%\end{figure}