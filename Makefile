SHELL := /bin/bash
LATEXCMD := pdflatex --shell-escape -halt-on-error
BIBTEXCMD := bibtex
INDXCMD := makeindex
TARGET := thesis

all: $(TARGET).pdf
.PHONY: $(TARGET).pdf

$(TARGET).pdf: *.tex
	$(LATEXCMD) $(TARGET)
	$(BIBTEXCMD) $(TARGET).aux
	$(INDXCMD) $(TARGET).nlo -s Latex/Classes/nomencl.ist -o $(TARGET).nls
	$(LATEXCMD) $(TARGET)
	$(LATEXCMD) $(TARGET)

clean:
	rm -f *.aux
	rm -f $(TARGET).blg
	rm -f $(TARGET).bbl
	rm -f $(TARGET).log
	rm -f $(TARGET).out
	rm -f $(TARGET).idx
	rm -f $(TARGET).ilg
	rm -f $(TARGET).lof
	rm -f $(TARGET).lot
	rm -f $(TARGET).nlo
	rm -f $(TARGET).nls
	rm -f $(TARGET).toc
	rm -f $(TARGET).brf
	# rm -f $(TARGET).pdf
